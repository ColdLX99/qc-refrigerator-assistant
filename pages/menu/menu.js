const $http = require('../../utils/http')
var util=require("../../utils/utilDay")
const {apiBaseUrl} = require('../../utils/config.js')

Page({
    data: {
        swiperImgs: [
        
        ],
        Height: "", //这是swiper要动态设置的高度属性,
        Imgs1: [
            'https://s21.ax1x.com/2024/06/23/pkrgpnS.jpg', 
             'https://s21.ax1x.com/2024/06/23/pkrglN9.jpg',
            'https://s21.ax1x.com/2024/06/23/pkrgK74.png',
            'https://s21.ax1x.com/2024/06/23/pkrg90g.jpg'
        ],
        week_best_picture:[] , 
        num:3,
    },
    imgHeight(e){
        var winWid = wx.getSystemInfoSync().windowWidth; //获取当前屏幕的宽度
        var imgh=e.detail.height;//图片高度
        var imgw=e.detail.width;//图片宽度
        var swiperH=winWid*imgh/imgw + "px"//等比设置swiper的高度。 即 屏幕宽度 / swiper高度 = 图片宽度 / 图片高度  ==》swiper高度 = 屏幕宽度 * 图片高度 / 图片宽度
        this.setData({
          Height:swiperH//设置高度
        })
    },
    fetchData() {
        wx.showLoading({
            title: '数据疯狂加载中...',
            mask: true,
        });
        return Promise.allSettled([
            $http({
                url: `${apiBaseUrl}dish/cook/1`,
            }),
            $http({
                url: `${apiBaseUrl}dish/cook/2`,
            }),
            $http({
                url: `${apiBaseUrl}dish/cook/3`,
            }),
            $http({
                url: `${apiBaseUrl}dish/cook/4`,
            }),
            $http({
                url: `${apiBaseUrl}dish/cook/5`,
            }),
        ]).then((results) => {                                         
            wx.hideLoading();
            // console.log(results[0].value.data.data);
            let dishPicture = [] ;
            for(let i = 0 ;i < 5 ; i ++ ) 
            {
                dishPicture[i] = results[i].value.data.data.dish
            }
            // console.log(dishPicture[0]);
            this.setData ({
                // week_best_picture: week_best,
                swiperImgs: dishPicture
            })
           
        })
    },
    //查看更多
    seeMore() {
        wx.navigateTo({
            url: '../Classification/Classification',
        })
    },
    //搜索食材 
    search_foods() {
        wx.navigateTo({
            url: './search_foods/searchFoods',
        })
    },
    // 获取本周佳作 
    handle_week_best(){ 
        let num = 3 ;
        // 当前周几 
        let time = util.formatDate(new Date());
        let date=util.getDates(7, time);
        num = date[0].week
        num = 120 ; 
        return Promise.allSettled([
            $http({
                url : `${apiBaseUrl}dish/commend/${num}`
            }),
        ]).then((res)=>{ 
            let week_best_picture = [] ; 
            
            // 目前没有办法解决 num + i 无限 懒加载 
            // 利用for 循环 
            
            
            // for(var i = 1 ; i <100; i ++) { 
            //     return Promise.allSettled([
            //         $http({
            //             url : `${apiBaseUrl}dish/commend/${num + i }`
            //         }),
            //     ]).then((res)=>{ 

            //     })
            // }
            week_best_picture.push(res[0].value.data.data)
            this.setData ({
                week_best_picture:week_best_picture
            })
        })
    },
    // 获取早中晚
    handleEarly(){ 
        wx.navigateTo({
          url: '../time_meal/time_meal',
        })
    },
    //精选菜谱 
    SelctMax() { 
        wx.navigateTo({
          url: '../Featured_Recipes/Featured_Recipes',
        })
    }, 
    //跳转本周佳作
    weekMax() { 

        wx.navigateTo({
          url: '../vegetable/vegetable?id=' + 120,
        })
     },
     //根据[id] 跳转对应轮播图 
     getLunBoList(e) { 
        // console.log(e);
        
        wx.navigateTo({
          url: '../vegetable/vegetable?id=' + e.currentTarget.dataset.id,
        })
     }, 
     //热门推荐点击  
     maxDoorBind(e) {
        
        let navigateToPage = e.currentTarget.dataset.id ; 
        /**
         *  'https://i.328888.xyz/2023/02/25/E28kw.md.jpeg', 
             'https://i.328888.xyz/2023/02/25/E2bTz.md.jpeg',
            'https://i.328888.xyz/2023/02/25/E2N9x.md.jpeg',
            'https://i.328888.xyz/2023/02/25/E2Ipa.md.jpeg'
         */
        console.log(e);
        let hotDoorParams = [] ; 
        // console.log(navigateToPage);
        if (navigateToPage== "https://s21.ax1x.com/2024/06/23/pkrgpnS.jpg") {
            return Promise.allSettled([
                $http({
                    url : `${apiBaseUrl}dish/types/5`
                }),
            ]).then((res)=>{
                // console.log(res[0].value.data.data);
                // console.log("凉菜");
                // 赋值 像 之前那个 一样 
                let params = res[0].value.data.data;  
                hotDoorParams = [] ; 
                let dish_dataJSON = JSON.stringify(params);
                wx.navigateTo({
                    url: '../selected_reault/selected_reault?id=' + dish_dataJSON,
                  })
            })
        }
        if (navigateToPage== "https://s21.ax1x.com/2024/06/23/pkrglN9.jpg") {
            return Promise.allSettled([
                $http({
                    url : `${apiBaseUrl}dish/types/6`
                }),
            ]).then((res)=>{
                // console.log(res[0].value.data.data);
                
                // 赋值 像 之前那个 一样 
                let params = res[0].value.data.data;  
                hotDoorParams = [] ; 
                let dish_dataJSON = JSON.stringify(params);
                wx.navigateTo({
                    url: '../selected_reault/selected_reault?id=' + dish_dataJSON,
                  })
            })
        }
        if (navigateToPage== "https://s21.ax1x.com/2024/06/23/pkrgK74.png") {
            return Promise.allSettled([
                $http({
                    url : `${apiBaseUrl}dish/types/7`
                }),
            ]).then((res)=>{
                // console.log(res[0].value.data.data);
                // 赋值 像 之前那个 一样 
                let params = res[0].value.data.data;  
                hotDoorParams = [] ; 
                let dish_dataJSON = JSON.stringify(params);
                wx.navigateTo({
                    url: '../selected_reault/selected_reault?id=' + dish_dataJSON,
                  })
            })
        }
        if (navigateToPage== "https://s21.ax1x.com/2024/06/23/pkrg90g.jpg") {
            return Promise.allSettled([
                $http({
                    url : `${apiBaseUrl}dish/types/5`
                }),
            ]).then((res)=>{
                // console.log(res[0].value.data.data);
                
                // 赋值 像 之前那个 一样 
                let params = res[0].value.data.data;  
                hotDoorParams = [] ; 
                let dish_dataJSON = JSON.stringify(params);
                wx.navigateTo({
                    url: '../selected_reault/selected_reault?id=' + dish_dataJSON,
                  })
            })
        }
        
        wx.navigateTo({
          url: '../selected_reault/selected_reault?id=' + hotDoorParams,
        })
       
     },
    onLoad: function (options) {
        this.fetchData();
        this.handle_week_best();
    },
    
})