const $http = require('../../../utils/http')
import Dialog from '@vant/weapp/dialog/dialog';
const {apiBaseUrl} = require('../../../utils/config.js')
Page({
    data: {
        qw_img: [],
        dish_data: [],
        placeholderContent: "请输入要搜索的菜谱、食材",
        searchContent: ""
    },
    searchData(e) {
        let in_value = e.detail.value
        wx.request({
            url: `${apiBaseUrl}dish/select/1/10/` + in_value,
            method: 'GET',
            header: {
                'content-type': 'application/json' // 默认值
            },
            success: (res => {
                //    console.log(res);
                
                let dish_data = res.data.data.records;
                //    console.log(dish_data);
                return this.setData({
                    dish_data: dish_data
                })
            })
        })
    },
    
    // 搜索
    search_input() {
        let dish_dataJSON = JSON.stringify(this.data.dish_data);
        if (dish_dataJSON == "[]") {
            Dialog.confirm({
                title: '提示',
                message: '暂无此菜品，请搜索其他内容',
              })
                .then(() => {
                  // on confirm
                })
                .catch(() => {
                  // on cancel
                });
        } else {
            wx.navigateTo({
                url: '../../selected_reault/selected_reault?id=' + dish_dataJSON,
            })
        }
        // console.log(e);
    },
    onLoad(options) {
        this.searchData()
    },


})