const $http = require('../../utils/http')
const {apiBaseUrl} = require('../../utils/config.js')
Page({
    data: {
         //总的
         picture :[] , 
         //分页
         showlist :[],
         page:1, 
         pagesize : 8 , 
         BJ_PARAMS:[],
        PARAMS : [],
        inputValue:"",
        dish_data:"",
        searchContent:"",
        placeholderContent: "请输入要搜索的菜谱、食材",
    },
    //触底事件
    onReachBottom () {
        let shu = this.data.page
        this.setData({
          page:shu+1
        })
        //当我们页面到达底部的时候,我们让page++,取出接下来食物的数组
        let num = (this.data.page-1)*this.data.pagesize   
        let num2 = num+this.data.pagesize
        let arr = this.data.picture
        let qielist = arr.slice(num,num2)
        let slist = this.data.showlist
        let newarr = slist.concat(qielist)
        console.log(newarr);
        this.setData({
          showlist:newarr
        })
      },
    fetchData() {
         this.setData({picture : this.data.BJ_PARAMS})
    },
    searchData(e) {
      let in_value = e.detail.value
      wx.request({
          url: `${apiBaseUrl}dish/select/1/10/` + in_value,
          method: 'GET',
          header: {
              'content-type': 'application/json' // 默认值
          },
          success: (res => {
              //    console.log(res);
              
              let dish_data = res.data.data.records;
                 console.log(dish_data);
              return this.setData({
                  dish_data: dish_data
              })
          })
      })
    },
    // 搜索
    search_input() {
      console.log(111);
      wx.navigateTo({
        url: '../menu/search_foods/searchFoods',
      })
    },
    //点击食物页面跳转
    navigaeToVegatables(e){
      console.log(e);
      //微信小程序自带的api ,可以跳转页面
        wx.navigateTo({
          url: '../vegetable/vegetable?id=' + e.currentTarget.dataset.id ,
        })
    },
    //初次进入这个页面触发的事件
    onLoad: function (options) {
        wx.showLoading({
            title: '加载中...',
          })
        setTimeout(() => {
            wx.hideLoading()
          }, 300)
        let BJ_PARAMS = JSON.parse(options.id)
        //刚开始
        /*
        
        let arr= [1,2,3,4,5,6];
        let res= arr.slice(1,3);    //[1,3)
            console.log(res);// 2 3 
        */
       //只取8条数据
        let qielist = BJ_PARAMS.slice(this.data.page-1,this.data.pagesize)
        console.log(qielist);
        this.setData({
            BJ_PARAMS, 
            showlist:qielist
        })
        this.fetchData();
    },
    //点击清除按钮
    cancel(){
        this.setData({
         inputValue: ''
        })
      },

})

