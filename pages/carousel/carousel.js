 const $http = require('../../utils/http')
 const {apiBaseUrl} = require('../../utils/config')

 Page({
     data: {
         autoplay: true,
         indicatorDots: true,
         interval: 3000,
         duration: 1200,
         typeName : " " , 
        //菜单列表
        typeList : [],
         images_list: [{
                 "id": 1,
                 "image_url": "./image/1.jpg",
                 "link": "../list/list"
             },
             {
                 "id": 2,
                 "image_url": "./image/2.jpg",
                 "link": "../mine/mine"
             },
             {
                 "id": 3,
                 "image_url": "./image/3.jpg",
                 "link": "../mine/mine"
             }
         ],
     },
     fetchData(){
        wx.showLoading({
            title: '加载中...',
            mask: true,
        });
        return Promise.allSettled([
            $http({
                url: `${apiBaseUrl}dish/select/1/10/` + this.data.typeName,
            }),
        ]).then((results) => {
            wx.hideLoading();
            // console.log(results[0]);
            let pic = results[0].value.data.data.records; 
            console.log(pic);
            this.setData({typeList : pic})
        })
         
     },
     // 事件处理函数
     bindViewTap() {
         wx.navigateTo({
             url: '../logs/logs',
         })
     },
     onLoad:function(options) {
         console.log("凉心哥哥的参数---" + options.id);
         this.setData({
            typeName: options.id
         })
        this.fetchData()
       
     },
  
 })