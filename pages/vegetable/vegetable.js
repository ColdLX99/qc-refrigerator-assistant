// pages/vegetable/vegetable.js
const {apiBaseUrl} = require('../../utils/config.js')
Page({

    /**
     * 页面的初始数据
     */
    data: {
        id: 0,
        dateTable: [],
        vegetableCook: [],
        describe: false,
    },
    fetchDate() {
        wx.request({
            url: `${apiBaseUrl}dish/cook/` + this.data.id,
            method: 'GET',
            header: {
                'content-type': 'application/json' // 默认值
            },
            success: (res => {
                // console.log(res);
                this.setData({
                    dateTable: res.data.data,
                    vegetableCook: res.data.data.dishIngredientsAndRecipes
                })
            }),
        })
    },

    /**
     * dish/types/{typeId}
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        // console.log(options.id);
        let param = options.id;
        this.setData({
            id: param
        })
        this.fetchDate();
    },
    
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function (res) {
        var that = this;
        // console.log(that.data)
        return {
          title:that.data.dateTable.dish.dishName,
          path:'pages/vegetable/vegetable?id=' + '120',
          imageUrl:that.data.dateTable.dish.dishPicture
        }
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },


})