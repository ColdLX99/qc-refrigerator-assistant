// pages/Classification/Classification.js
//day:2.15
const {apiBaseUrl} = require('../../utils/config')

Page({
    data: {
        //tab
        arrlist: [],
        //当前点击的索引
        active: 0,
        isclick: false,
        ischange: true,
        totalContent: []
    },
    changetab(e) {
        //当我们点击不同的页面，清空内容数组。
        this.data.totalContent = [];
        let typeId = e.currentTarget.dataset.typeid;
        // console.log(typeId);
        wx.request({
            url: `${apiBaseUrl}type/${typeId}`,
            success: (res) => {
                let data = res.data.data;
                // console.log(data);
                this.setData({
                    totalContent: data
                })
            }
        })
        this.setData({
            active: e.currentTarget.dataset.index
        })
    },
    //点击的分类跳转到相应的页面
    handleClick(e) {
        let param = e.currentTarget.dataset;
        wx.navigateTo({
            url: '../carousel/carousel?id=' + param.id,
        })
    },
    jumpSearch() {
        wx.navigateTo({
            url: '../menu/search_foods/searchFoods',
        })
    },
    //页面初次渲染调用接口
    onReady() {
        //tab栏接口
        wx.request({
            url: `${apiBaseUrl}type/1`,
            method: 'GET',
            header: {
                'content-type': 'application/json' // 默认值
            },
            success: (res => {
                var Tabdata = res.data.data;
                // console.log(this.data.arrlist)
                // console.log(Tabdata);
                return this.setData({
                    arrlist: Tabdata
                })
            })
        })
        //菜式接口，刚开始默认加载第一个页面的数据
        wx.request({
            url: `${apiBaseUrl}type/2`,
            method: 'GET',
            header: {
                'content-type': 'application/json' // 默认值
            },
            success: (res => {
                var vegetableListdata = res.data.data;
                // console.log(vegetableListdata);
                return this.setData({
                    totalContent: vegetableListdata
                })
            })
        })
    },

})