const app = getApp();
const defaultAvatarUrl = 'https://mmbiz.qpic.cn/mmbiz/icTdbqWNOwNRna42FI242Lcia07jQodd2FJGIYQfG0LAJGFxM4FbnQP6yfMxBgJ0F3YRqJCJ1aPAK2dQagdusBZg/0'
const {
  apiBaseUrl
} = require('../../../utils/config.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatarUrl: defaultAvatarUrl,
    theme: wx.getSystemInfoSync().theme,
    touxiang: 'https://manager.diandianxc.com/diandianxc/mrtx.png',
    icon_r: 'https://manager.diandianxc.com/mine/enter.png',
    id: "",
    oid: "",
    sid: "",
    createName: "",
    phone: "",
    sex: [{
        name: '1',
        value: '男',
        checked: 'true'
      },
      {
        name: '2',
        value: '女'
      }
    ],
    isSex: "1",
    showPlus: true,
    imagePath: '',
  },
  handleChooseAlbum() {
    //系统API，让用户在相册中选择图片（或者拍照）
    wx.chooseImage({
      success: (res) => {
        //1、取出路径
        const path = res.tempFilePaths[0]
        console.log(path);
        //2、设置imagePath
        this.setData({
          imagePath: path,
          showPlus: false
        })
      }
    })
  },
  onLoad() {
    wx.onThemeChange((result) => {
      this.setData({
        theme: result.theme
      })
    })

  },
  onUnload() {

    console.log(this.data.imagePath);
    app.globalData.myImgUrl = this.data.imagePath
  },
  onChooseAvatar(e) {
    const {
      avatarUrl
    } = e.detail
    this.setData({
      avatarUrl,
    })
  },
  onChooseAvatar2() {
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success(res) {
        console.log("成功", res);
        // this.uploadImage(res.tempFilePaths[0]);
        wx.uploadFile({ //把临时图片上传到服务器
          url: "	https://www.hualigs.cn/api/upload",
          filePath: res.tempFilePaths[0],
          file: res.tempFilePaths[0],
          name: 'file',
          formData: {
            app: 3,
          },
          success: (res) => {
            console.log(res);
            let updata = JSON.parse(res.data);
            console.log(updata);
            if (updata.message == 'ok!') {
              this.editorCtx.insertImage({
                src: updata.data.url,
                success: () => {
                  console.log('insert image success')
                }
              })
              console.log(this.editorCtx);
            }
          }
        })
      }
    })

  },
  submitAvator(res) {

  },
  //input id  
  inputId(e) {
    // console.log("e.detail.value" + e.detail.value);
    this.data.id = e.detail.value
  },
  //inputSid   
  inputSid(e) {
    // console.log("e.detail.value" + e.detail.value);
    this.data.sid = e.detail.value
  },
  //createName 
  createName(e) {
    // console.log("e.detail.value" + e.detail.value);
    this.data.createName = e.detail.value
  },
  //createName 
  inputPhone(e) {
    // console.log("e.detail.value" + e.detail.value);
    this.data.phone = e.detail.value
  },
  //单选按钮发生变化
  radioChange(e) {
    console.log(e.detail.value);
    var sexName = this.data.isSex
    this.setData({
      isSex: e.detail.value
    })
  },
  getOpenid() {
    // 登录
    wx.login({
      success: (res) => {
        //用户的code
        let userCode = res.code
        wx.request({
          url: `${apiBaseUrl}user/login/${userCode}`,
          success: res => {
            let dataObject = JSON.parse(res.data.data);
            let openid = dataObject.openid;
            console.log(openid);
            this.setData({
              oid: openid
            })
          }
        });
        console.log(userCode);
      }
    })
  },
  //表单提交
  formSubmit(e) {

    //如果头像不为空，那么我们此时就显示退出登录按钮
    if (app.globalData.myImgUrl != " ") {
      app.globalData.isDisplay = true;
    }
    console.log(app.globalData.isDisplay);
    if (this.data.id == "" || this.data.oid == "" || this.data.createName == "" || this.data.isSex == "" || this.data.phone == "" || this.data.address.city == "" || this.data.address.district == "" || this.data.address.province == "") {
      wx.showToast({
        title: '请输入你的信息',
        icon: 'none',
        duration: 1500
      })
    }
    console.log(this.data);

    console.log("id" + this.data.oid);
    wx.request({
      url: `${apiBaseUrl}user/add/${this.data.id}/${this.data.oid}/${this.data.sid}/${this.data.isSex}/${this.data.phone}/${this.data.address.city}/${this.data.address.district}/${this.data.address.province}/Fri Mar 31 19:36:08 CST 2023/${this.data.createName}/Fri Mar 31 19:36:08 CST 2023/adddd/aaa`,
      method: 'GET',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: (res => {
        console.log(res);
        if (res.data.code != 200) {
          // wx.showToast({
          //   title: '注册失败，请重新注册!',
          //   icon: 'none',
          //   duration: 1500
          // })
          wx.showToast({
            title: '注册成功！',
            icon: 'none',
            duration: 1500
          })
          wx.switchTab({
            url: '../../index/home/home',
          })
        } else {
          wx.showToast({
            title: '注册成功!',
          })
          wx.switchTab({
            url: '../../index/home/home',
          })
        }
        //
        app.globalData.isDisplay = 'block';
      })

    })

  },

  //模态框取消
  modalCancel() {
    wx.showToast({
      title: '取消提交',
      icon: 'none'
    })
    this.setData({
      modalHidden: true,
    })
  },

  //模态框确定
  modalConfirm: function (e) {
    this.setData({

    })
    wx.showToast({
      title: '提交成功',
      icon: 'success'
    })
    this.setData({
      modalHidden: true
    })
  },

})