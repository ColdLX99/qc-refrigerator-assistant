// pages/my/my.js
const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        // avatarUrl: "https://s2.loli.net/2023/04/18/UvGF2XHTQkp3AgB.png", //这里放了一张灰色头像图片
        nickName: "点击登录",
        info: { //
            adinfo: '1.0.1', // 版本号
            formatted: '>', // 我的步数
        },
        openid: '还没登录哟,还没获取openid哟',
        imgURL: '',
        // 刚开始默认false
        isDisplay: app.globalData.isDisplay
    },
    getUserProfile() {
        wx.getUserProfile({
            desc: '登录', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
            success: (res) => {
                // console.log(res.userInfo.nickName)
                // console.log(res.userInfo.avatarUrl)
                this.setData({
                    nickName: res.userInfo.nickName,
                    avatarUrl: res.userInfo.avatarUrl
                })
                wx.setStorage({ //数据缓存方法
                    key: 'nickName', //关键字，本地缓存中指定的key
                    data: res.userInfo.nickName, //缓存微信用户公开信息，
                    success: function () { //缓存成功后，输出提示
                        console.log('写入nickName缓存成功')
                    },
                    fail: function () { //缓存失败后的提示
                        console.log('写入nickName发生错误')
                    }
                })

                wx.setStorage({ //数据缓存方法
                    key: 'avatarUrl', //关键字，本地缓存中指定的key
                    data: res.userInfo.avatarUrl, //缓存微信用户公开信息，
                    success: function () { //缓存成功后，输出提示
                        console.log('写入avatarUrl缓存成功')
                    },
                    fail: function () { //缓存失败后的提示
                        console.log('写入avatarUrl发生错误')
                    }
                })
            }
        })
    },

    navigateToBind() {
        wx.navigateTo({
            url: './bindFridge/bindFridge',
        })
        console.log(this.data.openid);
    },
    //注销
    Logout(){
        app.globalData.myImgUrl="https://ts1.cn.mm.bing.net/th/id/R-C.5c80aa95fbd3954894716d1ec12f004c?rik=flmfJ2KO%2fcItUw&riu=http%3a%2f%2fpic.ntimg.cn%2ffile%2f20180425%2f25124298_172519481324_2.jpg&ehk=lCAbTESr6UfvpTHME8gHYXlarjxwHjs8Ny4ODFRWuT4%3d&risl=&pid=ImgRaw&r=0";
        // app.globalData.isDisplay=false;
        // console.log(app.globalData.isDisplay);
        this.setData({
            isDisplay:false,
            imgURL:'https://ts1.cn.mm.bing.net/th/id/R-C.5c80aa95fbd3954894716d1ec12f004c?rik=flmfJ2KO%2fcItUw&riu=http%3a%2f%2fpic.ntimg.cn%2ffile%2f20180425%2f25124298_172519481324_2.jpg&ehk=lCAbTESr6UfvpTHME8gHYXlarjxwHjs8Ny4ODFRWuT4%3d&risl=&pid=ImgRaw&r=0'
        })
    },
    /**
     * regester 注册 跳转
     */
    regester() {
        wx.navigateTo({
            url: './regester/regester',
        })
        if(app.globalData.imgURL!="")
        {
            console.log(111);
        }
    },
    onShow: function () {
        /**
         * 我们想把注册页面的图片传递给我的页面的头像展示，只需要先在app.js中定义一个全局变量
         * 我们给其命名myImgUrl然后要记住一点，如果要在其他页面设置app.js中的myImgUrl数据，必须是生命周期函数中才好使，自己定义的函数是不成功的，当我们点击注册页面上传图片的时候，点击登录，此时页面关闭，调用卸载页面函数的钩子，就在此刻，我们把这个获取图片的链接赋值给myImgUrl。紧接着在my页面设置头像的属性赋值myImgUrl即可，此时达到了同步
         */
        // console.log(this.data.imgURL);
        this.setData({
            imgURL: app.globalData.myImgUrl,
            isDisplay: app.globalData.isDisplay
        })
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})