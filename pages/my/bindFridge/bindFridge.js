// pages/my/bindFridge/bindFridge.js
const $http = require('../../../utils/http')
const {apiBaseUrl} = require('../../../utils/config')

Page({

    /**
     * 页面的初始数据
     */
    data: {
        avatarUrl: "https://s2.loli.net/2023/04/18/UvGF2XHTQkp3AgB.png", //这里放了一张灰色头像图片
        nickName: "食管家用户",
        userProfile: []
    },
    Data(e) {
        // console.log(e);
        wx.request({
            // url: `http://192.168.1.3:8080/user/selectfam/` + 1231,
            url: `${apiBaseUrl}user/selectfam/1231`,
            method: 'GET',
            header: {
                'content-type': 'application/json' // 默认值
            },
            success: (res => {
                console.log(res);
                let userProfile = res.data.data;

                console.log(userProfile);
                return this.setData({
                    userProfile: userProfile
                })
            })
        })
    },
    // 跳转到添加页面
    add() {
        wx.navigateTo({
            url: '../addFridge/addFridge',
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.Data();
        wx.showLoading({
            title: '加载中...',
        })
        setTimeout(() => {
            wx.hideLoading()
        }, 300)
    },

})