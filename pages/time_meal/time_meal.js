// pages/time_meal/time_meal.js
const $http = require('../../utils/http')
const {apiBaseUrl} = require('../../utils/config.js')
Page({
  /**
   * 页面的初始数据
   */
  data: {
      arrlist: [{
          name: "早餐",
          isclick: true
      }, {
          name: "午餐",
          isclick: false
      }, {
          name: "晚餐",
          isclick: false
      }],
      active: 0,
      isclick: false,
      tableList : [] 
  },
  changetab(e) {
    wx.showLoading({
        title: '加载中...',
      })
    setTimeout(() => {
        wx.hideLoading()
      }, 600)
      // console.log(e.currentTarget.dataset.index);  
      this.setData({
        active: e.currentTarget.dataset.index
      })
      // 点击改变文字颜色
      var index = e.currentTarget.dataset.index //获取被点击的tab序号
      var oldlist = this.data.arrlist //复制一份tab数组
  
      for (let i = 0; i < oldlist.length; i++) {
        oldlist[i].isclick = false
        oldlist[index].isclick = true;
      }
      this.setData({
        arrlist: oldlist
      })
    //  console.log(this.data.active );
      if(this.data.active == 0 ) { 
        return Promise.allSettled([
            $http({
                url : `${apiBaseUrl}dish/types/3`
            }),
        ]).then((res)=>{
            // console.log(res[0].value.data.data);
            this.setData({
                tableList: res[0].value.data.data
            })
        })
      }
      
      
      if (this.data.active == 1 ) { 
        return Promise.allSettled([
            $http({
                url : `${apiBaseUrl}dish/types/4`
            }),
        ]).then((res)=>{
            // console.log(res[0].value.data.data);
            this.setData({
                tableList: res[0].value.data.data
            })
        })
      }
       if (this.data.active == 2 ) { 
        return Promise.allSettled([
            $http({
                url : `${apiBaseUrl}dish/types/5`
            }),
        ]).then((res)=>{
            // console.log(res[0].value.data.data);
            this.setData({
                tableList: res[0].value.data.data
            })
        })
      }
    },
    onLoad(){ 
        wx.showLoading({
            title: '加载中...',
          })
        setTimeout(() => {
            wx.hideLoading()
          }, 600)
    
        if(this.data.active == 0 ) { 
            return Promise.allSettled([
                $http({
                    // 早餐 51- 
                    url : `${apiBaseUrl}dish/types/4`
                }),
            ]).then((res)=>{
                // console.log(res[0].value.data.data);
                this.setData({
                    tableList: res[0].value.data.data
                })
            })
          }
    },
  //搜索食材 
  search_foods() {
      wx.navigateTo({
          url: '../menu/search_foods/searchFoods',
      })
  },

 //触底加载 
//  onReachBottom () {
//     wx.showLoading({
//       title: '加载中',
//     })
//     setTimeout(function () {
//       wx.hideLoading()
//     }, 500)
//   },

})