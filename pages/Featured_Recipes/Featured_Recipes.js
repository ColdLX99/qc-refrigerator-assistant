// pages/menu/menu.js

Page({

    /**
     * 页面的初始数据
     */
    data: {
        arrlist: [{
                name: "本周热门",
                isclick: true
            }, {
                name: "川菜类",
                isclick: false
            }, {
                name: "创意类",
                isclick: false
            },
            {
                name: "海鲜类",
                isclick: false
            },
            {
                name: "烘焙类",
                isclick: false
            },
            // {
            //     name: "家常味",
            //     isclick: false
            // },
            // {
            //     name: "京味菜",
            //     isclick: false
            // },
            // {
            //     name: "凉菜类",
            //     isclick: false
            // },
            // {
            //     name: "靓汤",
            //     isclick: false
            // },
            // {
            //     name: "鲁菜类",
            //     isclick: false
            // },
            // {
            //     name: "美容养颜",
            //     isclick: false
            // },
            // {
            //     name: "面点",
            //     isclick: false
            // },
            // {
            //     name: "女王菜",
            //     isclick: false
            // },
            // {
            //     name: "肉类",
            //     isclick: false
            // },
            // {
            //     name: "素类",
            //     isclick: false
            // },
            // {
            //     name: "湘菜类",
            //     isclick: false
            // },
            // {
            //     name: "小资系",
            //     isclick: false
            // },
            // {
            //     name: "西餐类",
            //     isclick: false
            // },
            // {
            //     name: "粤菜类",
            //     isclick: false
            // },
            // {
            //     name: "鱼类",
            //     isclick: false
            // },
        ],
        textsContent: [{
                uid: 0,
                img: 'https://i.328888.xyz/2023/03/13/vzqqv.png',
                info: '蛋黄饼干嘎嘎香',
                saw:23.6,
                like:2345
            },
            {
                uid: 0,
                img: 'https://tse1-mm.cn.bing.net/th/id/OIP-C.18Y5NCxHRQuQBaMHCmJNEgHaFB?pid=ImgDet&rs=1',
                info: '卷饼',
                saw:43.6,
                like:4587
            },{
                uid:0,
                img:'https://tse1-mm.cn.bing.net/th/id/OIP-C.My1th0f5w5Xcxu3gYFY9_gHaGI?pid=ImgDet&rs=1',
                info:'点心',
                saw:84.7,
                like:5673
            },
            {
                uid: 1,
                img: 'https://i.328888.xyz/2023/03/21/amLuV.jpeg',
                info: '油焖大虾馋哭你~',
                saw:33.2,
                like:2597
            },
            {
                uid: 1,
                img: 'https://i.328888.xyz/2023/03/21/aBfDJ.jpeg',
                info: '麻辣豆腐',
                saw:98.2,
                like:7323
            },
            {
                uid: 1,
                img: 'https://i.328888.xyz/2023/03/21/aBDio.jpeg',
                info: '川菜',
                saw:25.4,
                like:8214
            },
            {
                uid: 2,
                img: 'https://i.328888.xyz/2023/03/21/amP2d.jpeg',
                info: '鸡蛋菜花大米饭套餐',
                saw:13.6,
                like:2458
            },
            {
                uid: 2,
                img: 'https://i.328888.xyz/2023/03/21/amTEb.jpeg',
                info: '麻辣拌',
                saw:93.6,
                like:2531
            },
            {
                uid: 2,
                img: 'https://ts1.cn.mm.bing.net/th/id/R-C.1abacac4a9f77f78f72d16a77045da3d?rik=DBcdwnt6LhLRLg&riu=http%3a%2f%2fpic.xcarimg.com%2fimg%2fxbb%2fpic%2f2018%2f07%2fc_1150626235b61bd03bb5bd22c4fb73d1.jpg&ehk=I4Z9AIGNtQi1dFdM5qT8rE%2bHWLTHBOUdu4ns%2byiLCz0%3d&risl=&pid=ImgRaw&r=0',
                info: '烧烤',
                saw:58.1,
                like:7753
            },
           
            {
                uid: 3,
                img: 'https://img.zcool.cn/community/01cb8a5c25fdbfa8012029ac5c40a1.jpg@1280w_1l_2o_100sh.jpg',
                info: '贝壳',
                saw:213.6,
                like:9359
            },
            {
                uid:3,
                img:"https://img.tukuppt.com/png_preview/00/40/88/uXUIORCBkH.jpg!/fw/780",
                info:"海鲜",
                saw:123.5,
                like:1434
            },
            {
                uid: 3,
                img: 'https://img95.699pic.com/photo/50088/0520.jpg_wh860.jpg',
                info: '大虾盛宴',
                saw:492.1,
                like:9943
            },
            {
                uid: 4,
                img: 'https://img.zcool.cn/community/01a3955e513f79a80120a895186bf0.jpg@1280w_1l_2o_100sh.jpg',
                info: '面包',
                saw:258.6,
                like:49212
            },
            {
                uid: 4,
                img: 'https://img.zcool.cn/community/0155bd5baf1429a801213dea0c37d4.jpg@1280w_1l_2o_100sh.jpg',
                info: '奶油面包',
                saw:163.6,
                like:9572
            },
            {
                uid: 4,
                img: 'https://img.zcool.cn/community/01dd09596a07d5a8012193a3094b8c.jpg@1280w_1l_2o_100sh.jpg',
                info: '面食',
                saw:312.6,
                like:59221
            },

        ],
        active: 0,
        isclick: false,
        qw_img: [],
        dish_data: [],
        placeholderContent: "请输入要搜索的菜谱、食材",
        searchContent: "",

    },
    changetab(e) {
        wx.showLoading({
            title: '加载中...',
          })
        setTimeout(() => {
            wx.hideLoading()
          }, 300)
        // console.log(e.currentTarget.dataset.index);  
        this.setData({
            active: e.currentTarget.dataset.index
        })
        // 点击改变文字颜色
        var index = e.currentTarget.dataset.index //获取被点击的tab序号
        var oldlist = this.data.arrlist //复制一份tab数组
        for (let i = 0; i < oldlist.length; i++) {
            oldlist[i].isclick = false
            oldlist[index].isclick = true;
        }
        this.setData({
            arrlist: oldlist
        })

    },
    onLoad(){ 
        wx.showLoading({
            title: '加载中...',
          })
        setTimeout(() => {
            wx.hideLoading()
          }, 300)
    },
    search() {
        wx.navigateTo({
            url: '../menu/search_foods/searchFoods',
            // ./search_foods/searchFoods
        })
    },

})