module.exports= (options) =>new Promise ((resolve,reject) => { 
  wx.request({
     ...options,
     success: (result)=>{
       resolve(result) 
     },
     fail: ()=>{
       reject(new Error ('网络异常，请稍后重试!')) ; 
     },
     complete: ()=>{}
   });
 })